# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 1.1.0
2020-06-14

### Features

- new gitlab-ci (ec378eb012f11e45f7a592f9ebf64e1422cdcc24)

### Fixes

- jest configuration (5e7ab6a69a8049db67ded44bb2273328d6c53bb8)

