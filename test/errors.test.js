'use strict'

const path = require('path')
const loader = require('../lib/index')()

describe('#index', () => {
  describe('#failure', () => {
    it('should throw if no arg is passed', () => {
      expect(() => loader()).toThrow()
    })

    it('should throw if a string is passed as arg', () => {
      expect(() => loader('test')).toThrow(Error)
    })

    it('should throw if array is empty', () => {
      expect(() => loader({})).toThrow(Error)
    })

    it('should throw if directory doesn\'t exist', () => {
      expect(() => loader([path.join(__dirname, 'unknow'), 'api'])).toThrow(Error)
    })
  })
})
