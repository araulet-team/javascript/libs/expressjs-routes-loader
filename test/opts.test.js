'use strict'

const routesFolder1 = `${__dirname}/fixture/usecase-1`
const routesFolder2 = `${__dirname}/fixture/usecase-2`

describe('#testing options', () => {
  describe('#success: adding more options should be ignored', () => {
    it('should succeded is nothing is passed', () => {
      const loader = require('../lib/index')()
      const result = loader([`${routesFolder1}`, 'api'])
      expect(result).toHaveLength(4)
    })

    it('should succeded if an empty object is passed', () => {
      const loader = require('../lib/index')({})
      const result = loader([`${routesFolder1}`, 'api'])
      expect(result).toHaveLength(4)
    })

    it('should ignore properties not supported', () => {
      const loader = require('../lib/index')({ hello: 'world' })
      const result = loader([`${routesFolder1}`, 'api'])
      expect(result).toHaveLength(4)
    })

    it('should use path in endpoint is useFolderName is set to true', () => {
      const loader = require('../lib/index')({ useFolderName: true })
      const result = loader([`${routesFolder1}`, 'api'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/hello/')
    })

    it('should use path in endpoint is useFolderName is set to true', () => {
      const loader = require('../lib/index')({ useFolderName: true })
      const result = loader([`${routesFolder2}`, 'api'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/v1/hello/')
    })
  })
})
