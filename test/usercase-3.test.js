'use strict'

const loader = require('../lib/index')()

describe('#load every routes from multiple nested directories.', () => {
  describe('#success: folder route without trailing ending slash', () => {
    let routesFolder

    beforeEach(() => {
      routesFolder = `${__dirname}/fixture/usecase-3`
    })

    it('should work when specifying one main folder', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(6)
    })

    it('should work when specifying prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(6)
      expect(result[0].url).toEqual('/api/import/')
    })

    it('should work when specifying multiple prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api', 'another'])
      expect(result).toHaveLength(6)
      expect(result[0].url).toEqual('/api/another/import/')
    })
  })

  describe('#success: folder route with trailing ending slash', () => {
    let routesFolder

    beforeEach(() => {
      routesFolder = `${__dirname}/fixture/usecase-3/`
    })

    it('should work when specifying one main folder', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(6)
    })

    it('should work when specifying prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(6)
      expect(result[0].url).toEqual('/api/import/')
    })

    it('should work when specifying multiple prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api', 'another'])
      expect(result).toHaveLength(6)
      expect(result[0].url).toEqual('/api/another/import/')
    })
  })
})
