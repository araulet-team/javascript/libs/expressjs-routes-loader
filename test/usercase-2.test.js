'use strict'

const loader = require('../lib/index')()

describe('#load every routes from multiple directories.', () => {
  describe('#success: folder route without trailing ending slash', () => {
    let routesFolder

    beforeEach(() => {
      routesFolder = `${__dirname}/fixture/usecase-2`
    })

    it('should work when specifying one main folder', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(4)
    })

    it('should work when specifying prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/hello/')
    })

    it('should work when specifying multiple prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api', 'another'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/another/hello/')
    })
  })

  describe('#success: folder route with trailing ending slash', () => {
    let routesFolder

    beforeEach(() => {
      routesFolder = `${__dirname}/fixture/usecase-2/`
    })

    it('should work when specifying one main folder', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(4)
    })

    it('should work when specifying prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/hello/')
    })

    it('should work when specifying multiple prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api', 'another'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/another/hello/')
    })
  })

  describe('#success: multiple folders injected', () => {
    let routesFolder1
    let routesFolder2

    beforeEach(() => {
      routesFolder1 = `${__dirname}/fixture/usecase-2/v1/`
      routesFolder2 = `${__dirname}/fixture/usecase-2/v2/`
    })

    it('should work when specifying one main folder', () => {
      const result = loader([
        [`${routesFolder1}`, 'api', 'v1'],
        [`${routesFolder2}`, 'api', 'v2']
      ])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/v1/hello/')
      expect(result[2].url).toEqual('/api/v2/users/')
    })
  })
})
