'use strict'

describe('#load every routes from a specific directory.', () => {
  describe('#failure: routes are duplicated', () => {
    let routesFolder

    beforeEach(() => {
      routesFolder = `${__dirname}/fixture/usecase-4`
    })

    it('should raise an error by default', () => {
      const loader = require('../lib/index')()
      expect(() => loader([`${routesFolder}`, 'api'])).toThrow(Error)
    })

    it('should not raise an error', () => {
      const loader = require('../lib/index')({ raiseErrorOnDuplicate: false })
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/hello/')
    })
  })
})
