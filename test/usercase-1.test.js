'use strict'

const loader = require('../lib/index')()

describe('#load every routes from a specific directory.', () => {
  describe('#success: folder route without trailing ending slash', () => {
    let routesFolder

    beforeEach(() => {
      routesFolder = `${__dirname}/fixture/usecase-1`
    })

    it('should work when specifying one main folder', () => {
      const result = loader([`${routesFolder}`])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/hello/')
    })

    it('should work when specifying prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/hello/')
    })

    it('should work when specifying multiple prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api', 'another'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/another/hello/')
    })

    it('should work when specifying empty prefix', () => {
      const result = loader([`${routesFolder}`, ''])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/hello/')
    })
  })

  describe('#success: folder route with trailing ending slash', () => {
    let routesFolder

    beforeEach(() => {
      routesFolder = `${__dirname}/fixture/usecase-1/`
    })

    it('should work when specifying one main folder', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(4)
    })

    it('should work when specifying prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/hello/')
    })

    it('should work when specifying multiple prefix for endpoint', () => {
      const result = loader([`${routesFolder}`, 'api', 'another'])
      expect(result).toHaveLength(4)
      expect(result[0].url).toEqual('/api/another/hello/')
    })
  })
})
