'use strict'

function post (req, res) {
  res.send()
}

function get (req, res) {
  res.send()
}

module.exports = [
  {
    method: 'post',
    url: 'users',
    handler: post,
    summary: 'users',
    description: 'users',
    tags: ['users']
  },
  {
    method: 'get',
    url: 'users/:id',
    handler: get,
    summary: 'Get users',
    description: 'Get users',
    tags: ['users']
  }
]
