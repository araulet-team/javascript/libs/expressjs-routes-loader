'use strict'

function post (req, res) {
  res.send()
}

function get (req, res) {
  res.send()
}

module.exports = [
  {
    method: 'post',
    url: '',
    handler: post,
    summary: 'user',
    description: 'user',
    tags: ['user']
  },
  {
    method: 'get',
    url: 'users/:id',
    handler: get,
    summary: 'Get user',
    description: 'Get user',
    tags: ['user']
  }
]
