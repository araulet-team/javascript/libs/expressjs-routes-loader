const fs = require('fs')
const debug = require('debug')('express-route-loader')
const path = require('path')

let defaultSettings = {
  raiseErrorOnDuplicate: true,
  useFolderName: false
}

/**
 * Configure and return route loader
 *
 * @param  {Object} opts   loader configuration
 * @return {Array<Object>}
 */
function setup (opts) {
  const _opts = Object.assign(defaultSettings, opts)

  debug(`options: ${_opts}`)

  /**
   * Get all routes with their normalized endpoints
   *
   * args
   * ----
   * Simple case: parse everything from this folder
   * [
   *   path.join(__dirname, 'src', 'routes'),
   *   'api',
   *   'v1'
   * ]
   *
   * Advanced case: parse different folders
   * [
   *   [
   *     path.join(__dirname, 'src', 'users'),
   *     'api',
   *     'v1',
   *     'users'
   *   ],
   *   [
   *     path.join(__dirname, 'src', 'routes', 'events'),
   *     'api',
   *     'events'
   *   ]
   * ]
   *
   * @param  {Array.<String...>|Array.<Array>} foldersToExplore   directories you want to explore
   * @return {Array.<Object>}
   */
  function load (foldersToExplore) {
    if (!Array.isArray(foldersToExplore) || foldersToExplore.length === 0) {
      throw new Error('argument should be an array not empty.')
    }

    if (!Array.isArray(foldersToExplore[0])) {
      foldersToExplore = [foldersToExplore]
    }

    return validateDuplicate(_explore(foldersToExplore))
  }

  /**
   * Explore file system given specific path
   *
   * @param  {Array<Array>} folders   all folders that needs to be explored
   * @return {Array<Object>}          all imported routes
   */
  function _explore (folders) {
    return folders
            .map(config => {
              const folder = config[0]
              const prefix = config.splice(1).join('/')

              return _requireFiles(folder)
                      .map(routes => _normalizeUrl(routes, prefix))
            })
            .reduce((acc, routes) => acc.concat(routes), [])
  }

  /**
   * Return all imported routes definition
   *
   * output
   * ------
   * [
   *   {
   *     method: 'post',
   *     url: 'hello',
   *     handler: [Function: post],
   *     summary: 'Hello',
   *     description: 'Hello',
   *     tags: [ 'hello' ],
   *     __relativePath__: '/v1/anotherlevel',
   *     __file__: <path to the file>
   *   },
   *   ...
   * ]
   *
   * @param  {Array.<String...>} dir      directory that needs to be parsed
   * @return {Array.<Object>}             all routes definition
   */
  function _requireFiles (dir) {
    return _browseDir(dir)
            .reduce((acc, file) => {
              const rtes = require(file)
              const relativePath = path.dirname(file).substring(dir.length)

              rtes.forEach(rte => {
                rte.__relativePath__ = relativePath
                rte.__file__ = file
              })

              acc = acc.concat(rtes)

              return acc
            }, [])
  }

  /**
   * Explore directories and extract files in recursive way.
   *
   * output
   * ------
   * [
   *   '/absolute/path/expressjs-routes-loader/test/routes/v1/anotherlevel/route1-1.js',
   *   '/absolute/path/expressjs-routes-loader/test/routes/route2.js'
   * ]
   *
   * @param {String} dir      absolute path directory
   * @return {Array.<String>} return all files path's
   */
  function _browseDir (dir) {
    try {
      return fs.readdirSync(dir)
                .map(item => _buildFullPath(dir, item))
                .map(item => _addFileOrContinueBrowsing(item))
                .reduce((acc, item) => acc.concat(item), [])
    } catch (e) {
      throw new Error(e.message)
    }
  }

  /** build absolute path for the required file */
  const _buildFullPath = (dir, name) => path.resolve(`${dir}/${name}`)

  /**
   * Return file or continue browsing
   *
   * @param {String} item               can be a file or directory
   * @return {String}
   */
  function _addFileOrContinueBrowsing (item) {
    return !fs.statSync(item).isDirectory() ? item : _browseDir(item)
  }

  /**
   * Build final normalized endpoints depending on previous configuration
   *
   * - add folder path if "useFolderName" is set to "true"
   * - add prefix to endpoint
   *
   * input
   * -----
   * [
   *   {
   *     method: 'GET',
   *     url: 'hello',
   *     handler: [Function: get],
   *     description: '...',
   *     tags: [...]
   *   }
   * ]
   *
   * output
   * ------
   * [
   *   {
   *     method: 'GET',
   *     url: '/api/hello', # addinf prefix to all routes if needed
   *     handler: [Function: get],
   *     description: '...',
   *     tags: [...]
   *   }
   * ]
   *
   * @param  {Object} route             route definition
   * @param  {String} prefix            prefix to be added
   * @return {Object}
   */
  function _normalizeUrl (route, prefix) {
    let _route = Object.assign({}, route)
    let url = _route.url
    let relativePath = _route.__relativePath__
    url = _addPrefixToUrl(url, relativePath, _opts.useFolderName)
    url = _addPrefixToUrl(url, prefix, true)
    _route.url = url

    debug(`rewritting url: ${url}`)

    return _route
  }

  /**
   * Add "/" to endpoint (begin and end)
   * Will return "/" for en empty string
   *
   * @param  {String} url
   * @return {String}
   */
  function _standardizeUrl (url) {
    if (!/^\//.test(url)) {
      url = `/${url}`
    }

    if (!/\/$/.test(url)) {
      url = `${url}/`
    }

    return url
  }

  /**
   * Add a prefix to an url
   *
   * @param {String}  url         current url/endpoint
   * @param {String}  prefix      prefix to add
   * @param {Boolean} isEnabled   only add if it's explicitly set
   * @return {String}
   */
  function _addPrefixToUrl (url, prefix, isEnabled) {
    url = _standardizeUrl(url)
    prefix = _standardizeUrl(prefix)

    return isEnabled ? `${prefix.replace(/\/$/, '')}${url}` : url
  }

  /**
   * Validate that you don't have duplicated endpoints when
   * importing your routes.
   * Throw an error if it's the case.
   *
   * @param  {Array<Object>} routes   all imported routes
   */
  function validateDuplicate (routes) {
    if (_opts.raiseErrorOnDuplicate) {
      const urls = routes.map(route => route.url)
      const uniq = [...new Set(urls)]

      if (urls.length !== uniq.length) {
        throw new Error('Your application has duplicated url.')
      }
    }

    return routes
  }

  return load
}

module.exports = setup
